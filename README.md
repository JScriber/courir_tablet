# Objectif du projet

La présente application permet la sélection de chaussure dans un catalogue. La chaussure sélectionnée est envoyée sur le miroir intelligent pour apparaitre aux pieds de l'utilisateur.
Sur cette application, le conseiller peut:

| Fonctionnalité                                        | Implémentation |
| ----------------------------------------------------- |:---------------|
| Scanner un QR code pour identifier le client          | ❌              |
| Consulter les informations du client                  | ❌              |
| Voir la liste des chaussures proposées par le magasin | ✅              |
| Sélectionner et envoyer une chaussure sur le miroir   | ✅              |
| Choisir la couleur de la chaussure                    | ✅              |
| Choisir la taille de la chaussure                     | ✅              |
| Ajouter la paire au panier utilisateur                | ✅              |
| Consulter et valider le panier                        | ❌              |

# Écran
## Capture vidéo

<img src="https://gitlab.com/JScriber/courir_tablet/-/wikis/uploads/cb013cb37a9685a6a1c72c998e798ee0/readme_courir.gif" height="400"/>

## Captures images

<img src="https://gitlab.com/JScriber/courir_tablet/-/wikis/uploads/83d2a67d677405bce4524f756269c4c4/capture1.png" height="400"/>

<img src="https://gitlab.com/JScriber/courir_tablet/-/wikis/uploads/de11235bb6a352b8c4f9b5f057b3eb4b/capture2.png" height="400"/>

<img src="https://gitlab.com/JScriber/courir_tablet/-/wikis/uploads/ef8c8141b0b063f35cc89a7ba13a30c7/capture3.png" height="400"/>

# Technologie

Le projet a été réalisé avec **Flutter**. L'application mobile est donc compatible avec Android et iOS.
