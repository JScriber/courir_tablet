import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class AppBarWidget extends StatelessWidget {
  final Function onOpenCart;

  AppBarWidget({this.onOpenCart});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(.2),
            offset: Offset(0, 1.0),
            blurRadius: 10.0,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 5.0,
          horizontal: 10.0,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              icon: new Icon(MdiIcons.faceProfile),
              onPressed: () {},
            ),
            Text(
              'Catalogue',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 17.0,
              ),
            ),
            Row(
              children: [
                IconButton(
                  icon: new Icon(MdiIcons.cart),
                  onPressed: onOpenCart,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
