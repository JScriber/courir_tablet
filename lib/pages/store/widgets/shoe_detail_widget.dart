import 'package:courir/models/cart_shoe.dart';
import 'package:courir/models/shoe.dart';
import 'package:courir/models/shoe_color.dart';
import 'package:courir/widgets/button_widget.dart';
import 'package:courir/widgets/color_choice_widget.dart';
import 'package:courir/widgets/label_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:numberpicker/numberpicker.dart';

class ShoeDetailWidget extends StatefulWidget {
  final Shoe shoe;

  final Function(CartShoe) onSelect;

  ShoeDetailWidget({@required this.shoe, @required this.onSelect});

  @override
  _ShoeDetailWidgetState createState() => _ShoeDetailWidgetState();
}

class _ShoeDetailWidgetState extends State<ShoeDetailWidget> {
  ShoeColor activeColor;

  int shoeSize = 30;

  @override
  void initState() {
    activeColor = widget.shoe.colors[widget.shoe.defaultColor];

    super.initState();
  }

  @override
  void didUpdateWidget(covariant ShoeDetailWidget oldWidget) {
    setState(() {
      activeColor = widget.shoe.colors[widget.shoe.defaultColor];
      shoeSize = 30;
    });

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        fit: StackFit.expand,
        children: [
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  color: Color(0xfff5f5f5),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 50, 0, 20),
                        child: Image.asset(
                          'assets/${widget.shoe.image.prefix}${activeColor.imageSuffix}.png',
                          width: 280.0,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: colorPickers(),
                      ),
                      Container(height: 40),
                    ],
                  ),
                ),
                Container(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.shoe.name,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 23.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Container(height: 5),
                      RatingBarIndicator(
                        itemSize: 20,
                        rating: widget.shoe.rating,
                        direction: Axis.horizontal,
                        itemCount: 5,
                        itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                      ),
                      LabelWidget(text: 'Pointure'),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 30.0,
                            vertical: 10.0,
                          ),
                          child: NumberPicker(
                            value: shoeSize,
                            minValue: 20,
                            maxValue: 42,
                            step: 2,
                            itemCount: 6,
                            axis: Axis.horizontal,
                            onChanged: (value) =>
                                setState(() => shoeSize = value),
                            itemHeight: 70,
                            itemWidth: 70,
                          ),
                        ),
                      ),
                      LabelWidget(text: 'Description'),
                      Text(
                        widget.shoe.description,
                        style: TextStyle(fontSize: 13),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    color: Color(0xfff5f5f5),
                    width: 1.0,
                  ),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 20.0,
                  horizontal: 15.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      widget.shoe.price,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.blue,
                      ),
                    ),
                    ButtonWidget(
                      text: 'Ajouter au panier',
                      onTap: () {
                        widget.onSelect(
                          CartShoe(
                            shoe: widget.shoe,
                            color: widget.shoe.colors.indexOf(activeColor),
                            size: shoeSize,
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> colorPickers() {
    List<Widget> widgets = [];

    for (var color in widget.shoe.colors) {
      widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: ColorChoice(
            color: color.color,
            selected: activeColor == color,
            onTap: () {
              setState(() {
                activeColor = color;
              });
            },
          ),
        ),
      );
    }

    return widgets;
  }
}
