import 'package:flutter/material.dart';

class NoShoeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.cast_connected,
            size: 45.0,
          ),
          Container(height: 10),
          ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: 300.0,
            ),
            child: Text(
              'Sélectionnez une chaussure pour la projeter sur le miroir',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
