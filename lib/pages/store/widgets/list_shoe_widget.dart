import 'package:after_layout/after_layout.dart';
import 'package:courir/models/shoe.dart';
import 'package:courir/network/shoe_api.dart';
import 'package:courir/pages/store/widgets/app_bar_widget.dart';
import 'package:courir/widgets/shoe_item_widget.dart';
import 'package:flutter/material.dart';

class ListShoeWidget extends StatefulWidget {
  final Function(int) onSelect;

  final Function onOpenCart;

  ListShoeWidget({this.onSelect, this.onOpenCart});

  @override
  _ListShoeWidgetState createState() => _ListShoeWidgetState();
}

class _ListShoeWidgetState extends State<ListShoeWidget>
    with AfterLayoutMixin<ListShoeWidget> {
  List<Shoe> _shoes;

  int _selectedId = -1;

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      _shoes = ShoeApi().getShoes();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if (_shoes != null) ...[
              Container(height: 50),
              Expanded(
                child: GridView.builder(
                  itemCount: _shoes.length,
                  padding: EdgeInsets.all(20),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5,
                    childAspectRatio: 2 / 3,
                    crossAxisSpacing: 5,
                    mainAxisSpacing: 5,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    return ShoeItemWidget(
                      selected: index == _selectedId,
                      shoe: _shoes[index],
                      onTap: () {
                        if (_selectedId != index) {
                          setState(() {
                            _selectedId = index;
                            widget.onSelect(_shoes[index].id);
                          });
                        }
                      },
                    );
                  },
                ),
              ),
            ],
          ],
        ),
        AppBarWidget(
          onOpenCart: widget.onOpenCart,
        ),
      ],
    );
  }
}
