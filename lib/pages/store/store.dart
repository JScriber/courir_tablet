import 'package:courir/models/cart_shoe.dart';
import 'package:courir/models/shoe.dart';
import 'package:courir/network/shoe_api.dart';
import 'package:courir/pages/store/widgets/list_shoe_widget.dart';
import 'package:courir/pages/store/widgets/no_shoe_widget.dart';
import 'package:courir/pages/store/widgets/shoe_detail_widget.dart';
import 'package:flutter/material.dart';

class StoreWidget extends StatefulWidget {
  @override
  _StoreWidgetState createState() => _StoreWidgetState();
}

class _StoreWidgetState extends State<StoreWidget> {
  Shoe _shoe;

  Set<CartShoe> _cart = Set();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Row(
          children: [
            Expanded(
              flex: 10,
              child: ListShoeWidget(
                onSelect: (int id) {
                  // TODO: Add loading as API may take some time.
                  setState(() {
                    _shoe = ShoeApi().getShoe(id);
                  });
                },
                onOpenCart: () {
                  showDialog(
                    context: context,
                    builder: (_) => new AlertDialog(
                      title: new Text("Panier"),
                      content: new Text("Hey! I'm Coflutter!"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('Fermer'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
            Expanded(
              flex: 5,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(width: 1, color: Color(0xfff5f5f5)),
                  Expanded(
                    child: _shoe == null
                        ? NoShoeWidget()
                        : ShoeDetailWidget(
                            shoe: _shoe,
                            onSelect: (CartShoe shoe) {
                              _selectCurrentShoe(context, shoe);
                            },
                          ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _selectCurrentShoe(BuildContext context, CartShoe shoe) {
    _cart.add(shoe);

    final snackBar =
        SnackBar(content: Text('${shoe.shoe.name} ajouté au panier'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
