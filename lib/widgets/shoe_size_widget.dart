import 'package:flutter/material.dart';

class ShoeSizeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _buildItem(),
        _buildItem(),
        _buildItem(),
        _buildItem(),
        _buildItem(),
        _buildItem(),
      ],
    );
  }

  Widget _buildItem() {
    return Column(
      children: [
        Container(
          height: 20,
          width: 20,
          decoration: BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.circular(20),
          ),
        ),
        Container(height: 5),
        Text(
          '28',
          style: TextStyle(fontWeight: FontWeight.w400),
        ),
      ],
    );
  }
}
