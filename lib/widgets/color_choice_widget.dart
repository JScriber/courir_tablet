import 'package:flutter/material.dart';

class ColorChoice extends StatelessWidget {
  final bool selected;

  final Color color;

  final void Function() onTap;

  ColorChoice({this.selected, this.color, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Stack(
        children: [
          if (this.selected) ...[
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(32),
                  border: Border.all(color: color, width: 2)),
              width: 32.0,
              height: 32.0,
            ),
          ],
          Padding(
            padding: const EdgeInsets.all(6.0),
            child: Container(
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(32),
              ),
              width: 20.0,
              height: 20.0,
            ),
          ),
        ],
      ),
    );
  }
}
