class ShoeImage {
  final String preview;

  final String prefix;

  ShoeImage({this.preview, this.prefix});
}
