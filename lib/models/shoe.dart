import 'package:courir/models/shoe_color.dart';
import 'package:courir/models/shoe_image.dart';

class Shoe {
  final int id;

  final String name;

  final String description;

  final String price;

  final double rating;

  final ShoeImage image;

  final List<ShoeColor> colors;

  final int defaultColor;

  Shoe({
    this.id,
    this.name,
    this.description,
    this.image,
    this.price,
    this.rating,
    this.colors,
    this.defaultColor,
  });
}
