import 'package:flutter/material.dart';

class ShoeColor {
  final String imageSuffix;

  final Color color;

  ShoeColor({this.imageSuffix, this.color});
}
