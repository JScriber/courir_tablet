import 'package:courir/models/shoe.dart';

class CartShoe {
  final Shoe shoe;

  final int color;

  final int size;

  CartShoe({this.shoe, this.color, this.size});
}
