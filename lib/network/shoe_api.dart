import 'package:courir/models/shoe.dart';
import 'package:courir/models/shoe_color.dart';
import 'package:courir/models/shoe_image.dart';
import 'package:flutter/material.dart';

class ShoeApi {
  static final ShoeApi _singleton = ShoeApi._internal();

  factory ShoeApi() {
    return _singleton;
  }

  ShoeApi._internal();

  // TODO: Plug to real API.
  List<Shoe> _shoes = [
    Shoe(
      id: 1,
      name: 'Nike Air VaporMax FlyKnit',
      description:
          'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce eu libero tortor. Nulla facilisi. Mauris ultricies massa ut dui porta imperdiet. ',
      price: '89.99€',
      colors: [
        ShoeColor(imageSuffix: 'blue', color: Color(0xff145f9e)),
        ShoeColor(imageSuffix: 'darkblue', color: Color(0xff3e108d)),
        ShoeColor(imageSuffix: 'green', color: Color(0xff007661)),
        ShoeColor(imageSuffix: 'red', color: Color(0xff8a424d)),
      ],
      rating: 4.0,
      defaultColor: 0,
      image: ShoeImage(
        prefix: 'shoe1_',
        preview: 'shoe1_blue.png',
      ),
    ),
    Shoe(
      id: 2,
      name: 'Nike Air Casper',
      description:
          'Sed porta, nisl nec condimentum fermentum, orci ante luctus nisl, sit amet aliquet ante erat eget lorem. Duis auctor sagittis lectus ut tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam faucibus, dolor id porta bibendum, turpis lectus dapibus nisl',
      price: '120.99€',
      colors: [
        ShoeColor(imageSuffix: 'red', color: Color(0xffca2f34)),
        ShoeColor(imageSuffix: 'blue', color: Color(0xff006edf)),
      ],
      rating: 2.0,
      defaultColor: 0,
      image: ShoeImage(
        prefix: 'shoe2_',
        preview: 'shoe2_red.png',
      ),
    ),
  ];

  /// Lists all the shoes.
  List<Shoe> getShoes() {
    return _shoes;
  }

  /// Finds a shoe by its ID.
  Shoe getShoe(int id) {
    return _shoes.singleWhere((shoe) => shoe.id == id);
  }
}
